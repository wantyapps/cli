#include <stdio.h>
#include <string.h>
int boot(char drvs[]) {
	for (int i = 0; i <= (strlen(drvs) -1); i++) {
		printf("DRV: %c\n", drvs[i]);
		printf("Trying DRV: %c\n", drvs[i]);
	};
	return 0;
}
void restart(int code) {
	if ( code == 1 ) {
		printf("Restarted because of a problem. (EXIT CODE 1)\n");
	} else {
		printf("Restarted at code: %d\n", code);
	};
}

void showCommands() {
	printf("COMMANDS\n");
}

int main() {
	char command[100];
	char drives[100] = "A";
	while (0 == 0) {
		printf(">"); // Why this interface?
		scanf("%s", command);
		if ( strcmp( command, "boot") == 0 ) {
			printf("Booting...\n");
			boot(drives);
		} else {
			for (int i = 0; i <= (strlen(command) -1); i++) {
				if ( command[i] == ':' ) {
					if ( strcmp(command, ":rs") == 0 ) {
						char yesno[100];
						printf("Are you sure? [Y/n]> ");
						scanf("%s", yesno);
						if ( strcmp(yesno, "y") == 0 ) {
							restart(0);
							return 0;
						} else {}; // This is not beautiful...
					} else if ( strcmp(command, ":add") == 0 ) {
						printf("ADD DRV\n");
						printf("DRV_NAME: ");
						char drv_name[100];
						scanf("%s", drv_name);
						drives[strlen(drives) +1] = drv_name[0];
					} else if ( strcmp(command, ":commands") == 0 ) {
						showCommands();
					} else {
						printf("Command not found.\n");
					};
				};
			};
		};
	};
}
